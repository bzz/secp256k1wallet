//
//  main.m
//  secp256k1wallet
//
//  Created by Mikhail Baynov on 07/01/16.
//  Copyright © 2016 Mikhail Baynov. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
