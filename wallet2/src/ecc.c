//
//  ecc.c
//  wallet
//
//  Created by Mikhail Baynov on 19/11/15.
//  Copyright © 2015 Mikhail Baynov. All rights reserved.
//

#include "ecc.h"

#include <stdio.h>
#include <string.h>
#include <stdlib.h>



static uECC_word_t curve_p[uECC_WORDS] = {0xFFFFFC2F, 0xFFFFFFFE, 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF};
static EccPoint curve_G = { \
    {0x16F81798, 0x59F2815B, 0x2DCE28D9, 0x029BFCDB, 0xCE870B07, 0x55A06295, 0xF9DCBBAC, 0x79BE667E}, \
    {0xFB10D4B8, 0x9C47D08F, 0xA6855419, 0xFD17B448, 0x0E1108A8, 0x5DA4FBFC, 0x26A3C465, 0x483ADA77}};

///used to decompress public keys
static uECC_word_t curve_b[uECC_WORDS] = {0x00000007, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000};


///used for signing
#define uECC_N_WORDS 8
static uECC_word_t curve_n[uECC_N_WORDS] = {0xD0364141, 0xBFD25E8C, 0xAF48A03B, 0xBAAEDCE6, 0xFFFFFFFE, 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF};





#pragma mark -
static void vli_bytesToNative(uint32_t *p_native, const uint8_t *p_bytes)
{
    unsigned i;
    for (i = 0; i < uECC_WORDS; ++i) {
        const uint8_t *p_digit = p_bytes + 4 * (uECC_WORDS - 1 - i);
        p_native[i] = ((uint32_t)p_digit[0] << 24) | ((uint32_t)p_digit[1] << 16) | ((
                                                                                      uint32_t)p_digit[2] << 8) | (uint32_t)p_digit[3];
    }
}
static void vli_nativeToBytes(uint8_t *p_bytes, const uint32_t *p_native)
{
    unsigned i;
    for (i = 0; i < uECC_WORDS; ++i) {
        uint8_t *p_digit = p_bytes + 4 * (uECC_WORDS - 1 - i);
        p_digit[0] = p_native[i] >> 24;
        p_digit[1] = p_native[i] >> 16;
        p_digit[2] = p_native[i] >> 8;
        p_digit[3] = p_native[i];
    }
}


#pragma mark - Arithmetics 1
#define EVEN(vli) (!(vli[0] & 1))

static void vli_set(uECC_word_t *p_dest, const uECC_word_t *p_src)
{
    wordcount_t i;
    for (i = 0; i < uECC_WORDS; ++i) {
        p_dest[i] = p_src[i];
    }
}
static void vli_clear(uECC_word_t *p_vli)
{
    wordcount_t i;
    for (i = 0; i < uECC_WORDS; ++i) {
        p_vli[i] = 0;
    }
}
/* Computes p_result = p_left + p_right, returning carry. Can modify in place. */
static uECC_word_t vli_add(uECC_word_t *p_result, uECC_word_t *p_left,
                           uECC_word_t *p_right)
{
    uECC_word_t l_carry = 0;
    wordcount_t i;
    for (i = 0; i < uECC_WORDS; ++i) {
        uECC_word_t l_sum = p_left[i] + p_right[i] + l_carry;
        if (l_sum != p_left[i]) {
            l_carry = (l_sum < p_left[i]);
        }
        p_result[i] = l_sum;
    }
    return l_carry;
}
/* Computes p_result = p_left - p_right, returning borrow. Can modify in place. */
static uECC_word_t vli_sub(uECC_word_t *p_result, uECC_word_t *p_left,
                           uECC_word_t *p_right)
{
    uECC_word_t l_borrow = 0;
    wordcount_t i;
    for (i = 0; i < uECC_WORDS; ++i) {
        uECC_word_t l_diff = p_left[i] - p_right[i] - l_borrow;
        if (l_diff != p_left[i]) {
            l_borrow = (l_diff > p_left[i]);
        }
        p_result[i] = l_diff;
    }
    return l_borrow;
}
/* Returns sign of p_left - p_right. */
static cmpresult_t vli_cmp(uECC_word_t *p_left, uECC_word_t *p_right)
{
    swordcount_t i;
    for (i = uECC_WORDS - 1; i >= 0; --i) {
        if (p_left[i] > p_right[i]) {
            return 1;
        } else if (p_left[i] < p_right[i]) {
            return -1;
        }
    }
    return 0;
}
/* Returns 1 if p_vli == 0, 0 otherwise. */
static uECC_word_t vli_isZero(const uECC_word_t *p_vli)
{
    wordcount_t i;
    for (i = 0; i < uECC_WORDS; ++i) {
        if (p_vli[i]) {
            return 0;
        }
    }
    return 1;
}
static uECC_word_t vli_testBit(const uECC_word_t *p_vli, bitcount_t p_bit)
{
    return (p_vli[p_bit >> uECC_WORD_BITS_SHIFT] & ((uECC_word_t)1 <<
                                                    (p_bit & uECC_WORD_BITS_MASK)));
}
/* Computes p_vli = p_vli >> 1. */
static void vli_rshift1(uECC_word_t *p_vli)
{
    uECC_word_t *l_end = p_vli;
    uECC_word_t l_carry = 0;
    
    p_vli += uECC_WORDS;
    while (p_vli-- > l_end) {
        uECC_word_t l_temp = *p_vli;
        *p_vli = (l_temp >> 1) | l_carry;
        l_carry = l_temp << (uECC_WORD_BITS - 1);
    }
}







#pragma mark - Arithmetics 2


static void muladd(uECC_word_t a, uECC_word_t b, uECC_word_t *r0, uECC_word_t *r1,
                   uECC_word_t *r2)
{
    uECC_dword_t p = (uECC_dword_t)a * b;
    uECC_dword_t r01 = ((uECC_dword_t)(*r1) << uECC_WORD_BITS) | *r0;
    r01 += p;
    *r2 += (r01 < p);
    *r1 = r01 >> uECC_WORD_BITS;
    *r0 = (uECC_word_t)r01;
}
static void mul2add(uECC_word_t a, uECC_word_t b, uECC_word_t *r0, uECC_word_t *r1,
                    uECC_word_t *r2)
{
    uECC_dword_t p = (uECC_dword_t)a * b;
    uECC_dword_t r01 = ((uECC_dword_t)(*r1) << uECC_WORD_BITS) | *r0;
    *r2 += (p >> (uECC_WORD_BITS * 2 - 1));
    p *= 2;
    r01 += p;
    *r2 += (r01 < p);
    *r1 = r01 >> uECC_WORD_BITS;
    *r0 = (uECC_word_t)r01;
}


#pragma mark -

static void vli_mult(uECC_word_t *p_result, uECC_word_t *p_left, uECC_word_t *p_right)
{
    uECC_word_t r0 = 0;
    uECC_word_t r1 = 0;
    uECC_word_t r2 = 0;
    
    wordcount_t i, k;
    
    /* Compute each digit of p_result in sequence, maintaining the carries. */
    for (k = 0; k < uECC_WORDS; ++k) {
        for (i = 0; i <= k; ++i) {
            muladd(p_left[i], p_right[k - i], &r0, &r1, &r2);
        }
        p_result[k] = r0;
        r0 = r1;
        r1 = r2;
        r2 = 0;
    }
    for (k = uECC_WORDS; k < uECC_WORDS * 2 - 1; ++k) {
        for (i = (k + 1) - uECC_WORDS; i < uECC_WORDS; ++i) {
            muladd(p_left[i], p_right[k - i], &r0, &r1, &r2);
        }
        p_result[k] = r0;
        r0 = r1;
        r1 = r2;
        r2 = 0;
    }
    
    p_result[uECC_WORDS * 2 - 1] = r0;
}


static void vli_square(uECC_word_t *p_result, uECC_word_t *p_left)
{
    uECC_word_t r0 = 0;
    uECC_word_t r1 = 0;
    uECC_word_t r2 = 0;
    
    wordcount_t i, k;
    
    for (k = 0; k < uECC_WORDS * 2 - 1; ++k) {
        uECC_word_t l_min = (k < uECC_WORDS ? 0 : (k + 1) - uECC_WORDS);
        for (i = l_min; i <= k && i <= k - i; ++i) {
            if (i < k - i) {
                mul2add(p_left[i], p_left[k - i], &r0, &r1, &r2);
            } else {
                muladd(p_left[i], p_left[k - i], &r0, &r1, &r2);
            }
        }
        p_result[k] = r0;
        r0 = r1;
        r1 = r2;
        r2 = 0;
    }
    
    p_result[uECC_WORDS * 2 - 1] = r0;
}



#pragma mark - Arithmetics 3

static void omega_mult(uint32_t *p_result, uint32_t *p_right)
{
    /* Multiply by (2^9 + 2^8 + 2^7 + 2^6 + 2^4 + 1). */
    uint32_t l_carry = 0;
    wordcount_t k;
    
    for (k = 0; k < uECC_WORDS; ++k) {
        uint64_t p = (uint64_t)0x3D1 * p_right[k] + l_carry;
        p_result[k] = (p & 0xffffffff);
        l_carry = p >> 32;
    }
    p_result[uECC_WORDS] = l_carry;
    
    p_result[1 + uECC_WORDS] = vli_add(p_result + 1, p_result + 1,
                                       p_right); /* add the 2^32 multiple */
}

static void vli_mmod_fast(uECC_word_t *p_result, uECC_word_t *p_product)
{
    uECC_word_t l_tmp[2 * uECC_WORDS];
    uECC_word_t l_carry;
    
    vli_clear(l_tmp);
    vli_clear(l_tmp + uECC_WORDS);
    
    omega_mult(l_tmp, p_product + uECC_WORDS); /* (Rq, q) = q * c */
    
    l_carry = vli_add(p_result, p_product, l_tmp); /* (C, r) = r + q       */
    vli_clear(p_product);
    omega_mult(p_product, l_tmp + uECC_WORDS); /* Rq*c */
    l_carry += vli_add(p_result, p_result, p_product); /* (C1, r) = r + Rq*c */
    
    while (l_carry > 0) {
        --l_carry;
        vli_sub(p_result, p_result, curve_p);
    }
    
    if (vli_cmp(p_result, curve_p) > 0) {
        vli_sub(p_result, p_result, curve_p);
    }
}

/* Computes p_result = (p_left * p_right) % curve_p. */
static void vli_modMult_fast(uECC_word_t *p_result, uECC_word_t *p_left,
                             uECC_word_t *p_right)
{
    uECC_word_t l_product[2 * uECC_WORDS];
    vli_mult(l_product, p_left, p_right);
    vli_mmod_fast(p_result, l_product);
}

/* Computes p_result = p_left^2 % curve_p. */
static void vli_modSquare_fast(uECC_word_t *p_result, uECC_word_t *p_left)
{
    uECC_word_t l_product[2 * uECC_WORDS];
    vli_square(l_product, p_left);
    vli_mmod_fast(p_result, l_product);
}





/* Counts the number of words in p_vli. */
static wordcount_t vli_numDigits(const uECC_word_t *p_vli, wordcount_t p_maxWords)
{
    swordcount_t i;
    /* Search from the end until we find a non-zero digit.
     We do it in reverse because we expect that most digits will be nonzero. */
    for (i = p_maxWords - 1; i >= 0 && p_vli[i] == 0; --i) {
    }
    
    return (i + 1);
}


/* Counts the number of bits required to represent p_vli. */
static bitcount_t vli_numBits(const uECC_word_t *p_vli, wordcount_t p_maxWords)
{
    uECC_word_t i;
    uECC_word_t l_digit;
    
    wordcount_t l_numDigits = vli_numDigits(p_vli, p_maxWords);
    if (l_numDigits == 0) {
        return 0;
    }
    
    l_digit = p_vli[l_numDigits - 1];
    for (i = 0; l_digit; ++i) {
        l_digit >>= 1;
    }
    
    return (((bitcount_t)(l_numDigits - 1) << uECC_WORD_BITS_SHIFT) + i);
}




#pragma mark - Arithmetics 4
#pragma mark - Arithmetics 5
#pragma mark - Arithmetics 6
#pragma mark - Arithmetics 7




/* Modify (x1, y1) => (x1 * z^2, y1 * z^3) */
static void apply_z(uECC_word_t *X1, uECC_word_t *Y1,
                    uECC_word_t *Z)
{
    uECC_word_t t1[uECC_WORDS];
    
    vli_modSquare_fast(t1, Z);    /* z^2 */
    vli_modMult_fast(X1, X1, t1); /* x1 * z^2 */
    vli_modMult_fast(t1, t1, Z);  /* z^3 */
    vli_modMult_fast(Y1, Y1, t1); /* y1 * z^3 */
}





/* Computes p_result = (p_left + p_right) % p_mod.
 Assumes that p_left < p_mod and p_right < p_mod, p_result != p_mod. */
static void vli_modAdd(uECC_word_t *p_result, uECC_word_t *p_left, uECC_word_t *p_right,
                       uECC_word_t *p_mod)
{
    uECC_word_t l_carry = vli_add(p_result, p_left, p_right);
    if (l_carry || vli_cmp(p_result, p_mod) >= 0) {
        /* p_result > p_mod (p_result = p_mod + remainder), so subtract p_mod to get remainder. */
        vli_sub(p_result, p_result, p_mod);
    }
}
/* Computes p_result = (p_left - p_right) % p_mod.
 Assumes that p_left < p_mod and p_right < p_mod, p_result != p_mod. */
static void vli_modSub(uECC_word_t *p_result, uECC_word_t *p_left, uECC_word_t *p_right,
                       uECC_word_t *p_mod)
{
    uECC_word_t l_borrow = vli_sub(p_result, p_left, p_right);
    if (l_borrow) {
        /* In this case, p_result == -diff == (max int) - diff.
         Since -x % d == d - x, we can get the correct result from p_result + p_mod (with overflow). */
        vli_add(p_result, p_result, p_mod);
    }
}
#if !defined(asm_modSub_fast)
#define vli_modSub_fast(result, left, right) vli_modSub((result), (left), (right), curve_p)
#endif



/* Double in place */
static void EccPoint_double_jacobian(uECC_word_t *X1, uECC_word_t *Y1,
                                     uECC_word_t *Z1)
{
    /* t1 = X, t2 = Y, t3 = Z */
    uECC_word_t t4[uECC_WORDS];
    uECC_word_t t5[uECC_WORDS];
    
    if (vli_isZero(Z1)) {
        return;
    }
    
    vli_modSquare_fast(t5, Y1);   /* t5 = y1^2 */
    vli_modMult_fast(t4, X1, t5); /* t4 = x1*y1^2 = A */
    vli_modSquare_fast(X1, X1);   /* t1 = x1^2 */
    vli_modSquare_fast(t5, t5);   /* t5 = y1^4 */
    vli_modMult_fast(Z1, Y1, Z1); /* t3 = y1*z1 = z3 */
    
    vli_modAdd(Y1, X1, X1, curve_p); /* t2 = 2*x1^2 */
    vli_modAdd(Y1, Y1, X1, curve_p); /* t2 = 3*x1^2 */
    if (vli_testBit(Y1, 0)) {
        uECC_word_t l_carry = vli_add(Y1, Y1, curve_p);
        vli_rshift1(Y1);
        Y1[uECC_WORDS - 1] |= l_carry << (uECC_WORD_BITS - 1);
    } else {
        vli_rshift1(Y1);
    }
    /* t2 = 3/2*(x1^2) = B */
    
    vli_modSquare_fast(X1, Y1);   /* t1 = B^2 */
    vli_modSub(X1, X1, t4, curve_p); /* t1 = B^2 - A */
    vli_modSub(X1, X1, t4, curve_p); /* t1 = B^2 - 2A = x3 */
    
    vli_modSub(t4, t4, X1, curve_p); /* t4 = A - x3 */
    vli_modMult_fast(Y1, Y1, t4);    /* t2 = B * (A - x3) */
    vli_modSub(Y1, Y1, t5, curve_p); /* t2 = B * (A - x3) - y1^4 = y3 */
}



/* P = (x1, y1) => 2P, (x2, y2) => P' */
static void XYcZ_initial_double(uECC_word_t *X1, uECC_word_t *Y1,
                                uECC_word_t *X2, uECC_word_t *Y2,
                                const uECC_word_t *p_initialZ)
{
    uECC_word_t z[uECC_WORDS];
    
    vli_set(X2, X1);
    vli_set(Y2, Y1);
    
    vli_clear(z);
    z[0] = 1;
    if (p_initialZ) {
        vli_set(z, p_initialZ);
    }
    
    apply_z(X1, Y1, z);
    
    EccPoint_double_jacobian(X1, Y1, z);
    
    apply_z(X2, Y2, z);
}


/* Input P = (x1, y1, Z), Q = (x2, y2, Z)
 Output P + Q = (x3, y3, Z3), P - Q = (x3', y3', Z3)
 or P => P - Q, Q => P + Q
 */
static void XYcZ_addC(uECC_word_t *X1, uECC_word_t *Y1,
                      uECC_word_t *X2, uECC_word_t *Y2)
{
    /* t1 = X1, t2 = Y1, t3 = X2, t4 = Y2 */
    uECC_word_t t5[uECC_WORDS];
    uECC_word_t t6[uECC_WORDS];
    uECC_word_t t7[uECC_WORDS];
    
    vli_modSub_fast(t5, X2, X1); /* t5 = x2 - x1 */
    vli_modSquare_fast(t5, t5);      /* t5 = (x2 - x1)^2 = A */
    vli_modMult_fast(X1, X1, t5);    /* t1 = x1*A = B */
    vli_modMult_fast(X2, X2, t5);    /* t3 = x2*A = C */
    vli_modAdd(t5, Y2, Y1, curve_p); /* t4 = y2 + y1 */
    vli_modSub_fast(Y2, Y2, Y1); /* t4 = y2 - y1 */
    
    vli_modSub_fast(t6, X2, X1); /* t6 = C - B */
    vli_modMult_fast(Y1, Y1, t6);    /* t2 = y1 * (C - B) */
    vli_modAdd(t6, X1, X2, curve_p); /* t6 = B + C */
    vli_modSquare_fast(X2, Y2);      /* t3 = (y2 - y1)^2 */
    vli_modSub_fast(X2, X2, t6); /* t3 = x3 */
    
    vli_modSub_fast(t7, X1, X2); /* t7 = B - x3 */
    vli_modMult_fast(Y2, Y2, t7);    /* t4 = (y2 - y1)*(B - x3) */
    vli_modSub_fast(Y2, Y2, Y1); /* t4 = y3 */
    
    vli_modSquare_fast(t7, t5);      /* t7 = (y2 + y1)^2 = F */
    vli_modSub_fast(t7, t7, t6); /* t7 = x3' */
    vli_modSub_fast(t6, t7, X1); /* t6 = x3' - B */
    vli_modMult_fast(t6, t6, t5);    /* t6 = (y2 + y1)*(x3' - B) */
    vli_modSub_fast(Y1, t6, Y1); /* t2 = y3' */
    
    vli_set(X1, t7);
}



#pragma mark -
/* Input P = (x1, y1, Z), Q = (x2, y2, Z)
 Output P' = (x1', y1', Z3), P + Q = (x3, y3, Z3)
 or P => P', Q => P + Q
 */
static void XYcZ_add(uECC_word_t *X1, uECC_word_t *Y1,
                     uECC_word_t *X2, uECC_word_t *Y2)
{
    /* t1 = X1, t2 = Y1, t3 = X2, t4 = Y2 */
    uECC_word_t t5[uECC_WORDS];
    
    vli_modSub_fast(t5, X2, X1); /* t5 = x2 - x1 */
    vli_modSquare_fast(t5, t5);      /* t5 = (x2 - x1)^2 = A */
    vli_modMult_fast(X1, X1, t5);    /* t1 = x1*A = B */
    vli_modMult_fast(X2, X2, t5);    /* t3 = x2*A = C */
    vli_modSub_fast(Y2, Y2, Y1); /* t4 = y2 - y1 */
    vli_modSquare_fast(t5, Y2);      /* t5 = (y2 - y1)^2 = D */
    
    vli_modSub_fast(t5, t5, X1); /* t5 = D - B */
    vli_modSub_fast(t5, t5, X2); /* t5 = D - B - C = x3 */
    vli_modSub_fast(X2, X2, X1); /* t3 = C - B */
    vli_modMult_fast(Y1, Y1, X2);    /* t2 = y1*(C - B) */
    vli_modSub_fast(X2, X1, t5); /* t3 = B - x3 */
    vli_modMult_fast(Y2, Y2, X2);    /* t4 = (y2 - y1)*(B - x3) */
    vli_modSub_fast(Y2, Y2, Y1); /* t4 = y3 */
    
    vli_set(X2, t5);
}

/* Computes p_result = (1 / p_input) % p_mod. All VLIs are the same size.
 See "From Euclid's GCD to Montgomery Multiplication to the Great Divide" */
static void vli_modInv(uECC_word_t *p_result, uECC_word_t *p_input, uECC_word_t *p_mod)
{
    uECC_word_t a[uECC_WORDS], b[uECC_WORDS], u[uECC_WORDS], v[uECC_WORDS];
    uECC_word_t l_carry;
    cmpresult_t l_cmpResult;
    
    if (vli_isZero(p_input)) {
        vli_clear(p_result);
        return;
    }
    
    vli_set(a, p_input);
    vli_set(b, p_mod);
    vli_clear(u);
    u[0] = 1;
    vli_clear(v);
    while ((l_cmpResult = vli_cmp(a, b)) != 0) {
        l_carry = 0;
        if (EVEN(a)) {
            vli_rshift1(a);
            if (!EVEN(u)) {
                l_carry = vli_add(u, u, p_mod);
            }
            vli_rshift1(u);
            if (l_carry) {
                u[uECC_WORDS - 1] |= HIGH_BIT_SET;
            }
        } else if (EVEN(b)) {
            vli_rshift1(b);
            if (!EVEN(v)) {
                l_carry = vli_add(v, v, p_mod);
            }
            vli_rshift1(v);
            if (l_carry) {
                v[uECC_WORDS - 1] |= HIGH_BIT_SET;
            }
        } else if (l_cmpResult > 0) {
            vli_sub(a, a, b);
            vli_rshift1(a);
            if (vli_cmp(u, v) < 0) {
                vli_add(u, u, p_mod);
            }
            vli_sub(u, u, v);
            if (!EVEN(u)) {
                l_carry = vli_add(u, u, p_mod);
            }
            vli_rshift1(u);
            if (l_carry) {
                u[uECC_WORDS - 1] |= HIGH_BIT_SET;
            }
        } else {
            vli_sub(b, b, a);
            vli_rshift1(b);
            if (vli_cmp(v, u) < 0) {
                vli_add(v, v, p_mod);
            }
            vli_sub(v, v, u);
            if (!EVEN(v)) {
                l_carry = vli_add(v, v, p_mod);
            }
            vli_rshift1(v);
            if (l_carry) {
                v[uECC_WORDS - 1] |= HIGH_BIT_SET;
            }
        }
    }
    
    vli_set(p_result, u);
}




static void EccPoint_mult(EccPoint *p_result, EccPoint *p_point,
                          const uECC_word_t *p_scalar, const uECC_word_t *p_initialZ,
                          bitcount_t p_numBits)
{
    /* R0 and R1 */
    uECC_word_t Rx[2][uECC_WORDS];
    uECC_word_t Ry[2][uECC_WORDS];
    uECC_word_t z[uECC_WORDS];
    
    bitcount_t i;
    uECC_word_t nb;
    
    vli_set(Rx[1], p_point->x);
    vli_set(Ry[1], p_point->y);
    
    XYcZ_initial_double(Rx[1], Ry[1], Rx[0], Ry[0], p_initialZ);
    
    for (i = p_numBits - 2; i > 0; --i) {
        nb = !vli_testBit(p_scalar, i);
        XYcZ_addC(Rx[1 - nb], Ry[1 - nb], Rx[nb], Ry[nb]);
        XYcZ_add(Rx[nb], Ry[nb], Rx[1 - nb], Ry[1 - nb]);
    }
    
    nb = !vli_testBit(p_scalar, 0);
    XYcZ_addC(Rx[1 - nb], Ry[1 - nb], Rx[nb], Ry[nb]);
    
    /* Find final 1/Z value. */
    vli_modSub_fast(z, Rx[1], Rx[0]); /* X1 - X0 */
    vli_modMult_fast(z, z, Ry[1 - nb]);   /* Yb * (X1 - X0) */
    vli_modMult_fast(z, z, p_point->x);   /* xP * Yb * (X1 - X0) */
    vli_modInv(z, z, curve_p);            /* 1 / (xP * Yb * (X1 - X0)) */
    vli_modMult_fast(z, z, p_point->y);   /* yP / (xP * Yb * (X1 - X0)) */
    vli_modMult_fast(z, z, Rx[1 - nb]);   /* Xb * yP / (xP * Yb * (X1 - X0)) */
    /* End 1/Z calculation */
    
    XYcZ_add(Rx[nb], Ry[nb], Rx[1 - nb], Ry[1 - nb]);
    
    apply_z(Rx[0], Ry[0], z);
    
    vli_set(p_result->x, Rx[0]);
    vli_set(p_result->y, Ry[0]);
}








#pragma mark - COMPRESSION-DECOMPRESSION


static void uECC_compress(const uint8_t p_publicKey[uECC_BYTES * 2],
                          uint8_t p_compressed[uECC_BYTES + 1])
{
    wordcount_t i;
    for (i = 0; i < uECC_BYTES; ++i) {
        p_compressed[i + 1] = p_publicKey[i];
    }
    p_compressed[0] = 2 + (p_publicKey[uECC_BYTES * 2 - 1] & 0x01);
}


/* Compute a = sqrt(a) (mod curve_p). */
static void mod_sqrt(uECC_word_t *a)
{
    bitcount_t i;
    uECC_word_t p1[uECC_WORDS] = {1};
    uECC_word_t l_result[uECC_WORDS] = {1};
    
    /* Since curve_p == 3 (mod 4) for all supported curves, we can
     compute sqrt(a) = a^((curve_p + 1) / 4) (mod curve_p). */
    vli_add(p1, curve_p, p1); /* p1 = curve_p + 1 */
    for (i = vli_numBits(p1, uECC_WORDS) - 1; i > 1; --i) {
        vli_modSquare_fast(l_result, l_result);
        if (vli_testBit(p1, i)) {
            vli_modMult_fast(l_result, l_result, a);
        }
    }
    vli_set(a, l_result);
}

void uECC_decompress(const uint8_t p_compressed[uECC_BYTES + 1],
                     uint8_t p_publicKey[uECC_BYTES * 2])
{
    EccPoint l_point;
    vli_bytesToNative(l_point.x, p_compressed + 1);
    
    vli_modSquare_fast(l_point.y, l_point.x); /* r = x^2 */
    vli_modMult_fast(l_point.y, l_point.y, l_point.x); /* r = x^3 */
    vli_modAdd(l_point.y, l_point.y, curve_b, curve_p); /* r = x^3 + b */
    
    mod_sqrt(l_point.y);
    
    if ((l_point.y[0] & 0x01) != (p_compressed[0] & 0x01)) {
        vli_sub(l_point.y, curve_p, l_point.y);
    }
    
    vli_nativeToBytes(p_publicKey, l_point.x);
    vli_nativeToBytes(p_publicKey + uECC_BYTES, l_point.y);
}


#pragma mark - GET PUBLIC KEY


void uECC_get_public_key64(const uint8_t p_privateKey[uECC_BYTES],
                           uint8_t p_publicKey[uECC_BYTES * 2])
{
    EccPoint l_public;
    uECC_word_t l_private[uECC_WORDS];
    
    vli_bytesToNative(l_private, p_privateKey);
    
    EccPoint_mult(&l_public, &curve_G, l_private, 0, vli_numBits(l_private, uECC_WORDS));
    
    vli_nativeToBytes(p_publicKey, l_public.x);
    vli_nativeToBytes(p_publicKey + uECC_BYTES, l_public.y);
}


/* Get decompressed public key from the private key */
void uECC_get_public_key65(const uint8_t p_privateKey[uECC_BYTES],
                           uint8_t p_publicKey[uECC_BYTES * 2 + 1])
{
    uint8_t *p = p_publicKey;
    p[0] = 0x04;
    uECC_get_public_key64(p_privateKey, p + 1);
}
/* Get compressed public key from the private key */
void uECC_get_public_key33(const uint8_t p_privateKey[uECC_BYTES],
                           uint8_t p_publicKey[uECC_BYTES + 1])
{
    uint8_t p_publicKey_long[uECC_BYTES * 2];
    uECC_get_public_key64(p_privateKey, p_publicKey_long);
    uECC_compress(p_publicKey_long, p_publicKey);
}



#pragma mark READ KEY


/* Returns the decompressed public key in p_publicKey */
static int uECC_read_pubkey(const uint8_t *publicKey, uint8_t *p_publicKey)
{
    if (publicKey[0] == 0x04) {
        memcpy(p_publicKey, publicKey + 1, uECC_BYTES * 2);
        return 1;
    }
    if (publicKey[0] == 0x02 || publicKey[0] == 0x03) { // compute missing y coords
        uECC_decompress(publicKey, p_publicKey);
        return 1;
    }
    // error
    return 0;
}

/* Check if the private key is not equal to 0 and less than the order
 Returns 1 if valid */
int uECC_isValid(uint8_t *p_key)
{
    uECC_word_t l_key[uECC_WORDS];
    vli_bytesToNative(l_key, p_key);
    
    return (!vli_isZero(l_key) && vli_cmp(curve_n, l_key) == 1);
}




#pragma mark SIGNING

//void hmac_sha256(const uint8_t *key, const uint32_t keylen, const uint8_t *msg,
//                 const uint32_t msglen, uint8_t *hmac)
//{
//    int i;
//    uint8_t buf[SHA256_BLOCK_LENGTH], o_key_pad[SHA256_BLOCK_LENGTH],
//    i_key_pad[SHA256_BLOCK_LENGTH];
//    SHA256_CTX ctx;
//    
//    memset(buf, 0, SHA256_BLOCK_LENGTH);
//    if (keylen > SHA256_BLOCK_LENGTH) {
//        sha256_Raw(key, keylen, buf);
//    } else {
//        memcpy(buf, key, keylen);
//    }
//    
//    for (i = 0; i < SHA256_BLOCK_LENGTH; i++) {
//        o_key_pad[i] = buf[i] ^ 0x5c;
//        i_key_pad[i] = buf[i] ^ 0x36;
//    }
//    
//    sha256_Init(&ctx);
//    sha256_Update(&ctx, i_key_pad, SHA256_BLOCK_LENGTH);
//    sha256_Update(&ctx, msg, msglen);
//    sha256_Final(buf, &ctx);
//    
//    sha256_Init(&ctx);
//    sha256_Update(&ctx, o_key_pad, SHA256_BLOCK_LENGTH);
//    sha256_Update(&ctx, buf, SHA256_DIGEST_LENGTH);
//    sha256_Final(hmac, &ctx);
//}
//
///* generate K in a deterministic way, according to RFC6979
// http://tools.ietf.org/html/rfc6979 */
//int uECC_generate_k_rfc6979_test(uint8_t *secret, const uint8_t *priv_key,
//                                 const uint8_t *hash)
//{
//    int i;
//    uint8_t v[32], k[32], bx[2 * 32], buf[32 + 1 + sizeof(bx)], z1[32];
//    uECC_word_t l_z1[uECC_WORDS];
//    uECC_word_t l_secret[uECC_WORDS];
//    
//    vli_bytesToNative(l_z1, hash);
//    while ( vli_cmp(curve_p, l_z1) != 1) {
//        vli_sub(l_z1, l_z1, curve_p);
//    }
//    vli_nativeToBytes(z1, l_z1);
//    
//    memcpy(bx, priv_key, 32);
//    memcpy(bx + 32, z1, 32);
//    
//    memset(v, 1, sizeof(v));
//    memset(k, 0, sizeof(k));
//    
//    memcpy(buf, v, sizeof(v));
//    buf[sizeof(v)] = 0x00;
//    memcpy(buf + sizeof(v) + 1, bx, 64);
//    hmac_sha256(k, sizeof(k), buf, sizeof(buf), k);
//    hmac_sha256(k, sizeof(k), v, sizeof(v), v);
//    
//    memcpy(buf, v, sizeof(v));
//    buf[sizeof(v)] = 0x01;
//    memcpy(buf + sizeof(v) + 1, bx, 64);
//    hmac_sha256(k, sizeof(k), buf, sizeof(buf), k);
//    hmac_sha256(k, sizeof(k), v, sizeof(k), v);
//    
//    memset(bx, 0, sizeof(bx));
//    
//    for (i = 0; i < 10000; i++) {
//        hmac_sha256(k, sizeof(k), v, sizeof(v), secret);
//        vli_bytesToNative(l_secret, secret);
//        if ( !vli_isZero(l_secret) && vli_cmp(curve_n, l_secret) == 1) {
//            return 0; // good number -> no error
//        }
//        
//        memcpy(buf, v, sizeof(v));
//        buf[sizeof(v)] = 0x00;
//        hmac_sha256(k, sizeof(k), buf, sizeof(v) + 1, k);
//        hmac_sha256(k, sizeof(k), v, sizeof(v), v);
//    }
//    // we generated 10000 numbers, none of them is good -> fail
//    return 1;
//}



/* -------- ECDSA code -------- */

#define vli_modInv_n vli_modInv
#define vli_modAdd_n vli_modAdd

static void vli2_rshift1(uECC_word_t *p_vli)
{
    vli_rshift1(p_vli);
    p_vli[uECC_WORDS - 1] |= p_vli[uECC_WORDS] << (uECC_WORD_BITS - 1);
    vli_rshift1(p_vli + uECC_WORDS);
}

static uECC_word_t vli2_sub(uECC_word_t *p_result, uECC_word_t *p_left,
                            uECC_word_t *p_right)
{
    uECC_word_t l_borrow = 0;
    wordcount_t i;
    for (i = 0; i < uECC_WORDS * 2; ++i) {
        uECC_word_t l_diff = p_left[i] - p_right[i] - l_borrow;
        if (l_diff != p_left[i]) {
            l_borrow = (l_diff > p_left[i]);
        }
        p_result[i] = l_diff;
    }
    return l_borrow;
}

/* Computes p_result = (p_left * p_right) % curve_n. */
static void vli_modMult_n(uECC_word_t *p_result, uECC_word_t *p_left,
                          uECC_word_t *p_right)
{
    uECC_word_t l_product[2 * uECC_WORDS];
    uECC_word_t l_modMultiple[2 * uECC_WORDS];
    uECC_word_t l_tmp[2 * uECC_WORDS];
    uECC_word_t *v[2] = {l_tmp, l_product};
    
    vli_mult(l_product, p_left, p_right);
    vli_set(l_modMultiple + uECC_WORDS,
            curve_n); /* works if curve_n has its highest bit set */
    vli_clear(l_modMultiple);
    
    bitcount_t i;
    uECC_word_t l_index = 1;
    for (i = 0; i <= uECC_BYTES * 8; ++i) {
        uECC_word_t l_borrow = vli2_sub(v[1 - l_index], v[l_index], l_modMultiple);
        l_index = !(l_index ^ l_borrow); /* Swap the index if there was no borrow */
        vli2_rshift1(l_modMultiple);
    }
    
    vli_set(p_result, v[l_index]);
}

static bitcount_t smax(bitcount_t a, bitcount_t b)
{
    return (a > b ? a : b);
}



/* ECDSA signature.
 Returns 0 always. */
int uECC_sign_digest(const uint8_t p_privateKey[uECC_BYTES],
                     const uint8_t p_hash[uECC_BYTES],
                     uint8_t p_signature[uECC_BYTES * 2])
{
    uECC_word_t k[uECC_N_WORDS];
    uECC_word_t l_tmp[uECC_N_WORDS];
    uECC_word_t s[uECC_N_WORDS];
    uECC_word_t *k2[2] = {l_tmp, s};
    EccPoint p;
    uint8_t k_b[32];
    
    do {
    repeat:
        // Deterministic K
///        uECC_generate_k_rfc6979_test(k_b, p_privateKey, p_hash);
///FILL k with random bytes
        
//        uECC_word_t k[uECC_N_WORDS];
//        uECC_word_t l_tmp[uECC_N_WORDS];
#warning FILL k with random bytes
        
        
        
        vli_bytesToNative(k, k_b);
        
        if (vli_isZero(k)) {
            goto repeat;
        }
        
        if (vli_cmp(curve_n, k) != 1) {
            goto repeat;
        }
        
        /* make sure that we don't leak timing information about k. See http://eprint.iacr.org/2011/232.pdf */
        uECC_word_t l_carry = vli_add(l_tmp, k, curve_n);
        vli_add(s, l_tmp, curve_n);
        
        /* p = k * G */
        EccPoint_mult(&p, &curve_G, k2[!l_carry], 0, (uECC_BYTES * 8) + 1);
        
        /* r = x1 (mod n) */
        if (vli_cmp(curve_n, p.x) != 1) {
            vli_sub(p.x, p.x, curve_n);
        }
    } while (vli_isZero(p.x));
    
    
    /* Prevent side channel analysis of vli_modInv() to determine
     bits of k / the private key by premultiplying by a random number */

///    random_init();
///    random_bytes((uint8_t *)l_tmp, sizeof(l_tmp) / 2, 0); // call random once to improve speed
///FILL l_tmp with random bytes
#warning FILL k with random bytes
    
    // multiplies by a 16 instead of 32 byte number
    vli_modMult_n(k, k, l_tmp); /* k' = rand * k */
    vli_modInv_n(k, k, curve_n); /* k = 1 / k' */
    vli_modMult_n(k, k, l_tmp); /* k = 1 / k */
    
    vli_nativeToBytes(p_signature, p.x); /* store r */
    
    l_tmp[uECC_N_WORDS - 1] = 0;
    vli_bytesToNative(l_tmp, p_privateKey); /* tmp = d */
    s[uECC_N_WORDS - 1] = 0;
    vli_set(s, p.x);
    vli_modMult_n(s, l_tmp, s); /* s = r*d */
    
    vli_bytesToNative(l_tmp, p_hash);
    vli_modAdd_n(s, l_tmp, s, curve_n); /* s = e + r*d */
    vli_modMult_n(s, s, k); /* s = (e + r*d) / k */
    vli_nativeToBytes(p_signature + uECC_BYTES, s);
    
    return 0;
}


int uECC_verify_digest(const uint8_t *publicKey,
                       const uint8_t p_hash[uECC_BYTES],
                       const uint8_t p_signature[uECC_BYTES * 2])
{
    uint8_t p_publicKey[uECC_BYTES * 2];
    
    if (!uECC_read_pubkey(publicKey, p_publicKey)) {
        return 1;
    }
    
    uECC_word_t u1[uECC_N_WORDS], u2[uECC_N_WORDS];
    uECC_word_t z[uECC_N_WORDS];
    EccPoint l_public, l_sum;
    uECC_word_t rx[uECC_WORDS];
    uECC_word_t ry[uECC_WORDS];
    uECC_word_t tx[uECC_WORDS];
    uECC_word_t ty[uECC_WORDS];
    uECC_word_t tz[uECC_WORDS];
    
    uECC_word_t r[uECC_N_WORDS], s[uECC_N_WORDS];
    r[uECC_N_WORDS - 1] = 0;
    s[uECC_N_WORDS - 1] = 0;
    
    vli_bytesToNative(l_public.x, p_publicKey);
    vli_bytesToNative(l_public.y, p_publicKey + uECC_BYTES);
    vli_bytesToNative(r, p_signature);
    vli_bytesToNative(s, p_signature + uECC_BYTES);
    
    if (vli_isZero(r) || vli_isZero(s)) {
        /* r, s must not be 0. */
        return 1;
    }
    
    /* Calculate u1 and u2. */
    vli_modInv_n(z, s, curve_n); /* Z = s^-1 */
    u1[uECC_N_WORDS - 1] = 0;
    vli_bytesToNative(u1, p_hash);
    vli_modMult_n(u1, u1, z); /* u1 = e/s */
    vli_modMult_n(u2, r, z); /* u2 = r/s */
    
    /* Calculate l_sum = G + Q. */
    vli_set(l_sum.x, l_public.x);
    vli_set(l_sum.y, l_public.y);
    vli_set(tx, curve_G.x);
    vli_set(ty, curve_G.y);
    vli_modSub_fast(z, l_sum.x, tx); /* Z = x2 - x1 */
    XYcZ_add(tx, ty, l_sum.x, l_sum.y);
    vli_modInv(z, z, curve_p); /* Z = 1/Z */
    apply_z(l_sum.x, l_sum.y, z);
    
    /* Use Shamir's trick to calculate u1*G + u2*Q */
    EccPoint *l_points[4] = {0, &curve_G, &l_public, &l_sum};
    bitcount_t l_numBits = smax(vli_numBits(u1, uECC_N_WORDS), vli_numBits(u2, uECC_N_WORDS));
    
    EccPoint *l_point = l_points[(!!vli_testBit(u1, l_numBits - 1)) | ((!!vli_testBit(u2,
                                                                                      l_numBits - 1)) << 1)];
    vli_set(rx, l_point->x);
    vli_set(ry, l_point->y);
    vli_clear(z);
    z[0] = 1;
    
    bitcount_t i;
    for (i = l_numBits - 2; i >= 0; --i) {
        EccPoint_double_jacobian(rx, ry, z);
        
        uECC_word_t l_index = (!!vli_testBit(u1, i)) | ((!!vli_testBit(u2, i)) << 1);
        l_point = l_points[l_index];
        if (l_point) {
            vli_set(tx, l_point->x);
            vli_set(ty, l_point->y);
            apply_z(tx, ty, z);
            vli_modSub_fast(tz, rx, tx); /* Z = x2 - x1 */
            XYcZ_add(tx, ty, rx, ry);
            vli_modMult_fast(z, z, tz);
        }
    }
    
    vli_modInv(z, z, curve_p); /* Z = 1/Z */
    apply_z(rx, ry, z);
    
    /* v = x1 (mod n) */
    if (vli_cmp(curve_n, rx) != 1) {
        vli_sub(rx, rx, curve_n);
    }
    
    /* Accept only if v == r. */
    return !(vli_cmp(rx, r) == 0); // 0 on success
}










typedef struct {
    uint32_t d[8];
} secp256k1_scalar;

static void bzz_scalar_get_b32(unsigned char *bin, const secp256k1_scalar* a) {
    bin[0] = a->d[7] >> 24; bin[1] = a->d[7] >> 16; bin[2] = a->d[7] >> 8; bin[3] = a->d[7];
    bin[4] = a->d[6] >> 24; bin[5] = a->d[6] >> 16; bin[6] = a->d[6] >> 8; bin[7] = a->d[6];
    bin[8] = a->d[5] >> 24; bin[9] = a->d[5] >> 16; bin[10] = a->d[5] >> 8; bin[11] = a->d[5];
    bin[12] = a->d[4] >> 24; bin[13] = a->d[4] >> 16; bin[14] = a->d[4] >> 8; bin[15] = a->d[4];
    bin[16] = a->d[3] >> 24; bin[17] = a->d[3] >> 16; bin[18] = a->d[3] >> 8; bin[19] = a->d[3];
    bin[20] = a->d[2] >> 24; bin[21] = a->d[2] >> 16; bin[22] = a->d[2] >> 8; bin[23] = a->d[2];
    bin[24] = a->d[1] >> 24; bin[25] = a->d[1] >> 16; bin[26] = a->d[1] >> 8; bin[27] = a->d[1];
    bin[28] = a->d[0] >> 24; bin[29] = a->d[0] >> 16; bin[30] = a->d[0] >> 8; bin[31] = a->d[0];
}

static int bzz_ecdsa_sig_serialize(unsigned char *sig, size_t *size, unsigned char ar[32], unsigned char as[32]) {
    unsigned char r[33] = {0}, s[33] = {0};
    unsigned char *rp = r, *sp = s;
    size_t lenR = 33, lenS = 33;
    
    memcpy(&r[1], ar, 32);
    memcpy(&s[1], as, 32);
    
 //   printf("%s\n",s[1]);
//    secp256k1_scalar_get_b32(&r[1], ar);
//    secp256k1_scalar_get_b32(&s[1], as);
//    while (lenR > 1 && rp[0] == 0 && rp[1] < 0x80) { lenR--; rp++; }
//    while (lenS > 1 && sp[0] == 0 && sp[1] < 0x80) { lenS--; sp++; }
//    if (*size < 6+lenS+lenR) {
//        *size = 6 + lenS + lenR;
//        return 0;
//    }
//    *size = 6 + lenS + lenR;
//    sig[0] = 0x30;
//    sig[1] = 4 + lenS + lenR;
//    sig[2] = 0x02;
//    sig[3] = lenR;
    memcpy(sig+4, rp, lenR);
//    sig[4+lenR] = 0x02;
//    sig[5+lenR] = lenS;
    memcpy(sig+lenR+6, sp, lenS);
    return 1;
}





/*
 Signature Length: (48h = 72 bytes)
 48
 ECDSA Signature (X.690 DER-encoded):
 ASN.1 tag identifier (20h = constructed + 10h = SEQUENCE and SEQUENCE OF):
 30
 DER length octet, definite short form (45h = 69 bytes) (Signature r+s length)
 45
 ASN.1[/url] tag identifier (02 = INTEGER):
 02
 Signature r length (DER length octet):
 20
 Signature r (unsigned binary int, big-endian):
 263325fcbd579f5a3d0c49aa96538d9562ee41dc690d50dcc5a0af4ba2b9efcf
 ASN.1[/url] tag identifier (02 = INTEGER):
 02
 Signature s length (DER length octet):
 21
 Signature s (first byte is 00 pad to protect MSB 1 unsigned int):
 00fd8d53c6be9b3f68c74eed559cca314e718df437b5c5c57668c5930e14140502
 Signature end byte (SIGHASH_ALL):
 01
*/
typedef struct {
    uint8_t pushdata47;
    uint8_t sequence30;
    uint8_t length44or45;  //or 46?
    uint8_t integer02_1;
    uint8_t length20_1;
    //+00 if next byte's 7th bit set
    uint8_t ecc_r[20];
    uint8_t integer02_2;
    uint8_t length20_2;
    //+1?
    uint8_t ecc_s[20];
} der_signatute_type;




int DER_encode_digest(const uint8_t signature[uECC_BYTES * 2],
                            size_t  *der_signature_len,
                            uint8_t **der_signature)
{
    
    
    *der_signature_len = uECC_BYTES * 2;
    
    
    uECC_word_t r[uECC_N_WORDS], s[uECC_N_WORDS];
    r[uECC_N_WORDS - 1] = 0;
    s[uECC_N_WORDS - 1] = 0;
    vli_bytesToNative(r, signature);
    vli_bytesToNative(s, signature + uECC_BYTES);

    
    uint8_t ecc_r[32] = "\x77\x99\x99\x99\x99\x99\x99\x99\x99\x99\x99\x99\x99\x99\x99\x99\x99\x99\x99\x99\x99\x99\x99\x99\x99\x99\x99\x99\x99\x99\x99\x99\x99\x99\x99\x99\x99\x99\x99\x99\x99\x99";
    uint8_t ecc_s[32] = "\x77\x99\x99\x99\x99\x99\x99\x99\x99\x99\x99\x99\x99\x99\x99\x99\x99\x99\x99\x99\x99\x99\x99\x99\x99\x99\x99\x99\x99\x99\x99\x99\x99\x99\x99\x99\x99\x99\x99\x99\x99\x99";
    *der_signature = malloc(400);
    bzz_ecdsa_sig_serialize(&der_signature, der_signature_len, ecc_r, ecc_s);

//    vli_nativeToBytes(ecc_r, r);
    
    char *str = (char *)malloc(65);
//    char str[400] = {0};

    
//    uint8_t *str = (uint8_t *)"\x48\x30\x45\x02\x21";
//    *der_signature = str;

    
    uint8_t x = 0xfa;
    //0x6f
    if (x & (1<<7)) {
        ++der_signature_len;
        
        
//        *der_signature = malloc(400);
//        uint8_t *word = *der_signature;
//        //word[0] = 0x47;
//        word[1] = 0x30;
//        //
//        word[3] = 0x02;
        //        If the highest bit of the most significant byte is set, then you need to put 00 in front of it.
//        NSLog(@"7-th bit is set");
    } else {
//        sprintf(str, "\x48\x30\x45\x02\x21");
//        NSLog(@"7-th bit is not set");
    }

    return 0;
}



