#ifndef __TX_H
#define __TX_H

#include <stdint.h>
#include "common.h"
#include "endian.h"
#include "varint.h"
#include "vdata.h"



typedef struct {
    uint64_t value;
    uint64_t script_len;
    uint8_t *script;
} bbp_txout_t;

typedef struct {
    uint8_t txid[32];
    uint32_t index;
} bbp_outpoint_t;

typedef struct {
    bbp_outpoint_t outpoint;
    uint64_t script_len;
    uint8_t *script;
    uint32_t sequence;
} bbp_txin_t;

typedef struct {
    uint32_t version;
    uint64_t inputs_len;
    bbp_txin_t *inputs;
    uint64_t outputs_len;
    bbp_txout_t *outputs;
    uint32_t locktime;
} bbp_tx_t;

typedef enum {
    BBP_SIGHASH_ALL = 0x01
} bbp_sighash_t;




size_t bbp_tx_size(const bbp_tx_t *tx, bbp_sighash_t flag) {
    size_t size = 0;
    int i;

    /* version */
    size += sizeof(uint32_t);

    /* inputs count */
    size += bbp_varint_size(tx->inputs_len);

    /* inputs */
    for (i = 0; i < tx->inputs_len; ++i) {
        bbp_txin_t *txin = &tx->inputs[i];

        /* outpoint */
        size += sizeof(bbp_outpoint_t);

        /* script */
        size += bbp_varint_size(txin->script_len);
        size += txin->script_len;

        /* sequence */
        size += sizeof(uint32_t);
    }

    /* outputs count */
    size += bbp_varint_size(tx->outputs_len);

    /* outputs */
    for (i = 0; i < tx->outputs_len; ++i) {
        bbp_txout_t *txout = &tx->outputs[i];

        /* value */
        size += sizeof(uint64_t);

        /* script */
        size += bbp_varint_size(txout->script_len);
        size += txout->script_len;
    }

    /* locktime */
    size += sizeof(uint32_t);

    if (flag) {

        /* sighash */
        size += sizeof(uint32_t);
    }

    return size;
}

void bbp_tx_serialize(const bbp_tx_t *tx, uint8_t *raw, bbp_sighash_t flag) {
    uint8_t *ptr;
    size_t varlen;
    int i;

    ptr = raw;

    /* version */
    *(uint32_t *)ptr = tx->version;
    ptr += sizeof(uint32_t);

    /* inputs count */
    bbp_varint_set(ptr, tx->inputs_len, &varlen);
    ptr += varlen;

    /* inputs */
    for (i = 0; i < tx->inputs_len; ++i) {
        bbp_txin_t *txin = &tx->inputs[i];

        /* outpoint */
        memcpy(ptr, txin->outpoint.txid, 32);
        ptr += 32;
        *(uint32_t *)ptr = txin->outpoint.index;
        ptr += sizeof(uint32_t);

        /* script */
        bbp_varint_set(ptr, txin->script_len, &varlen);
        ptr += varlen;
        memcpy(ptr, txin->script, txin->script_len);
        ptr += txin->script_len;

        /* sequence */
        *(uint32_t *)ptr = txin->sequence;
        ptr += sizeof(uint32_t);
    }

    /* outputs count */
    bbp_varint_set(ptr, tx->outputs_len, &varlen);
    ptr += varlen;

    /* outputs */
    for (i = 0; i < tx->outputs_len; ++i) {
        bbp_txout_t *txout = &tx->outputs[i];

        /* value */
        *(uint64_t *)ptr = txout->value;
        ptr += sizeof(uint64_t);

        /* script */
        bbp_varint_set(ptr, txout->script_len, &varlen);
        ptr += varlen;
        memcpy(ptr, txout->script, txout->script_len);
        ptr += txout->script_len;
    }

    /* locktime */
    *(uint32_t *)ptr = tx->locktime;
    ptr += sizeof(uint32_t);

    if (flag) {

        /* sighash */
        *(uint32_t *)ptr = flag;
    }
}

#endif
