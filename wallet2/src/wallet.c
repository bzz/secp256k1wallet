#include "wallet.h"
#include "common.h"
#include "tx.h"

#include "vdata.h"
#include "create_tx.h"

#include "ecc.h"

#include "tests.h"


#include "postman.h"


void main_runloop1()
{
   networking_main();
}


void main_runloop()
{
   
   
   
   vdata priv_key = vdata_from_hex("119f56f7b969f8118e7e68cdc825966814d4ef61c09b3b2823ae5b0a76359746"); //"msaMCB38e227VRKbk3FEXocCmJLcN4tJJw"
   vdata change_addr_priv_key = priv_key;

   
   uint64_t fee = 15000;
   
   btc_txinput inputs[] = {
      {26760000, 0, vdata_from_hex("31b3d03ed8c2ec77863ba3bb7ee2c5cebabf9988e1fe8a5e77d41e39f89c0391")},
      {50000, 1, vdata_from_hex("31b3d03ed8c2ec77863ba3bb7ee2c5cebabf9988e1fe8a5e77d41e39f89c0391")},
      {50000, 2, vdata_from_hex("31b3d03ed8c2ec77863ba3bb7ee2c5cebabf9988e1fe8a5e77d41e39f89c0391")},
   };
    //1. change txin "...." to last tx for http://tbtc.blockr.io/address/info/msaMCB38e227VRKbk3FEXocCmJLcN4tJJw
    //2. sum = Raw (bottom)
    
   
   
   
   btc_txoutput outputs[] = {
      {50000, btc_hash160_from_addr("msaMCB38e227VRKbk3FEXocCmJLcN4tJJw")},
      {50000, btc_hash160_from_addr("msaMCB38e227VRKbk3FEXocCmJLcN4tJJw")},
      {25000, btc_hash160_from_addr("msKxGtZDoXeWjnxPNJbvZCBmLVv1fNqQMd")},
      {15000, btc_hash160_from_addr("mpL9RRBhJZp5T3rprkun5mefk61uX6Cy4p")},
   };

   
   vdata signed_message = btc_tx_create(priv_key, change_addr_priv_key, fee, inputs, 3, outputs, 4);

   vdata_print_hex("SIGNED MESSAGE", signed_message);

   
   if (is_vdata_equal(signed_message, vdata_from_hex("010000000391039cf8391ed4775e8afee18899bfbacec5e27ebba33b8677ecc2d83ed0b331000000008b483045022100e45e6fa15821ff994ffe9c7584efee62702a9e2af76ecaa927d1d1e9c6157df202202305daae947da4097c2a94d0bb184a4d48f1af1ae7f823934e60d4e47cd5a52b01410461d6e29695d5ac7575cdc876368fee939b4f003798599d76208f57f5ebc5cf2f2dab9c59a2179c98159b991a412fef237535ac167b88c4c6bdaff791f792ed05ffffffff91039cf8391ed4775e8afee18899bfbacec5e27ebba33b8677ecc2d83ed0b331010000008b483045022100ab4f446f4358ffb6f2868a00f517ec753ab672d278e132fafe21b33c093f1db502206e019371c719ed5e01fe9c154f5fe7f4647f3fa635442e3462e57d5db584d22e01410461d6e29695d5ac7575cdc876368fee939b4f003798599d76208f57f5ebc5cf2f2dab9c59a2179c98159b991a412fef237535ac167b88c4c6bdaff791f792ed05ffffffff91039cf8391ed4775e8afee18899bfbacec5e27ebba33b8677ecc2d83ed0b331020000008a4730440220636acef689fd917f169ee34ba3d6380766432cd2a62d318da6c3039ee70fcf3f02202fbbf40e9d1ad8b21202574d6105435c8f0aee922371d02651b5636a1a0689c901410461d6e29695d5ac7575cdc876368fee939b4f003798599d76208f57f5ebc5cf2f2dab9c59a2179c98159b991a412fef237535ac167b88c4c6bdaff791f792ed05ffffffff05687c9701000000001976a9148445df4193c05dd9be6c005f9c6d878ad1af0beb88ac50c30000000000001976a9148445df4193c05dd9be6c005f9c6d878ad1af0beb88ac50c30000000000001976a9148445df4193c05dd9be6c005f9c6d878ad1af0beb88aca8610000000000001976a914818ceac4fe31bb96a8ae556647668091807a652b88ac983a0000000000001976a91460add8446bfbc10f35d16923dcc11816610af05288ac00000000"))) {
      printf("SUCCESS");
   } else printf("FAIL!!!!!!!");
   
}









