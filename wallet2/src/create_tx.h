#ifndef create_tx_h
#define create_tx_h

#include "ecc.h"
#include "ecc_s.h"
#include "vdata.h"
#include "common.h"
#include "tx.h"
#include "btc_hash.h"




vdata btc_get_pubkey65(vdata priv_key) {
   vdata pubkey65 = VDATA_NEW(65);
   uECC_get_public_key65(priv_key.bytes, pubkey65.bytes);
   /*
    //pubkey from priv_key - secp256k1 lib version
    secp256k1_pubkey pk;
    size_t len = 65;
    uint8_t bytes[65];
    secp256k1_ec_pubkey_create(_ctx, &pk, priv_key.bytes);
    secp256k1_ec_pubkey_serialize(_ctx, bytes, &len, &pk, SECP256K1_EC_UNCOMPRESSED);
    vdata vpk = vdata_from_bytes(bytes, len);
    vdata_print_hex("     vpk from priv_key", vpk);
    */
   return pubkey65;
}




vdata signature_for_message_and_key(vdata message, uint8_t key[32])
{
   uint8_t *hash[32];
   hash256(message.bytes, message.len, hash);
   vdata out = VDATA_NEW(100);
    
   secp256k1_ecdsa_signature s;
   bzz_ecdsa_sign(&s, &hash, key, NULL);
        
    
   bzz_ecdsa_signature_serialize_der(out.bytes, &out.len, &s);
   return out;
}





vdata btc_tx_create(vdata priv_key,
                    vdata change_addr_priv_key,
                    uint64_t fee,
                    btc_txinput  *inputs,
                    size_t inputs_count,
                    btc_txoutput *outputs,
                    size_t outputs_count)
{

   
   bbp_txout_t outs[outputs_count+1];
   bbp_txout_t prev_outs[inputs_count];
   bbp_txin_t ins_sign[inputs_count];
   bbp_txin_t ins[inputs_count];
   bbp_tx_t tx;
   uint8_t *msg;
   size_t msg_len;
   vdata der_signature;
   
   
   

   
   vdata pubkey65 = btc_get_pubkey65(priv_key);
    
   vdata change_addr_pubkey65 = btc_get_pubkey65(change_addr_priv_key);
   uint8_t hash[20];
   hash160(change_addr_pubkey65.bytes, pubkey65.len, hash);
   vdata change_addr_hash160 = vdata_from_bytes(hash, 20);

   const char *change_addr = vdata_get_hex(change_addr_hash160);



   char *script = malloc(1000);

   
   uint64_t change = 0;
   for (size_t i=0; i<inputs_count; ++i) {
      prev_outs[i].value = inputs[i].balance;
      sprintf(script, "76a914%s88ac", change_addr);
      prev_outs[i].script_len = strlen(script);
      prev_outs[i].script = bbp_alloc_hex(script, (size_t *)&prev_outs[i].script_len);
      change += inputs[i].balance;
   }
   for (size_t i=0; i<outputs_count; ++i) {
      outs[i+1].value = outputs[i].amount;
      sprintf(script, "76a914%s88ac", vdata_get_hex(vdata_from_bytes(outputs[i].hash160.bytes, BTC_HASH160_LEN)));
      outs[i+1].script_len = strlen(script);
      outs[i+1].script = bbp_alloc_hex(script, (size_t *)&outs[i+1].script_len);
      change -= outputs[i].amount;
   }
   change -= fee;

   outs[0].value = change;
   sprintf(script, "76a914%s88ac", change_addr);
   outs[0].script_len = strlen(script);
   outs[0].script = bbp_alloc_hex(script, (size_t *)&outs[0].script_len);

   
   

   /* message */
   tx.version = 1;
   tx.outputs_len = outputs_count+1; //+1 for change
   tx.inputs_len = inputs_count;
   tx.locktime = 0;
   tx.outputs = outs;

   

   
   
   
   for (size_t i=0; i<inputs_count; ++i) {
      for (size_t n=0; n<32; ++n) {
         ins_sign[i].outpoint.txid[n] = inputs[i].id.bytes[31-n];   //reverse copy
         ins[i].outpoint.txid[n] = inputs[i].id.bytes[31-n];
      }
      ins_sign[i].outpoint.index = inputs[i].index;
      ins_sign[i].sequence = 0xffffffff;
      ins[i].outpoint.index = inputs[i].index;
      ins[i].sequence = 0xffffffff;
  }

   
   
   
   for (size_t i=0; i<inputs_count; ++i) {
      for (size_t n=0; n<inputs_count; ++n) {
         ins_sign[n].script_len = 0;
         ins_sign[n].script = "";
      }
      ins_sign[i].script_len = prev_outs[i].script_len;
      ins_sign[i].script = malloc(prev_outs[i].script_len);
      memcpy(ins_sign[i].script, prev_outs[i].script, prev_outs[i].script_len);

      tx.inputs = ins_sign;
      msg_len = bbp_tx_size(&tx, BBP_SIGHASH_ALL);
      msg = malloc(msg_len);
      bbp_tx_serialize(&tx, msg, BBP_SIGHASH_ALL);
      der_signature = signature_for_message_and_key(vdata_from_bytes(msg, msg_len), priv_key.bytes);
      sprintf(script, "%02lx%s%02x%02lx%s", der_signature.len + 1, vdata_get_hex(der_signature), BBP_SIGHASH_ALL, pubkey65.len, vdata_get_hex(pubkey65));
      ins[i].script_len = strlen(script);
      ins[i].script = bbp_alloc_hex(script, (size_t *)&ins[i].script_len);
   }
   
   
   
   
   
   /* packing */
   tx.outputs = outs;
   tx.inputs = ins;


   uint8_t *rawtx;
   size_t rawtx_len;
   rawtx_len = bbp_tx_size(&tx, 0);
   rawtx = malloc(rawtx_len);
   bbp_tx_serialize(&tx, rawtx, 0);
   
////   /* txid (print big-endian) */
////   uint8_t txid[32];
////   bbp_hash256(txid, rawtx, rawtx_len);
////   bbp_reverse(txid, 32);

   
   return vdata_from_bytes(rawtx, rawtx_len);
}



















#endif /* create_tx_h */
