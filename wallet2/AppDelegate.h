//
//  AppDelegate.h
//  secp256k1wallet
//
//  Created by Mikhail Baynov on 07/01/16.
//  Copyright © 2016 Mikhail Baynov. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

