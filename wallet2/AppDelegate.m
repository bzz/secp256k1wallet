//
//  AppDelegate.m
//  secp256k1wallet
//
//  Created by Mikhail Baynov on 07/01/16.
//  Copyright © 2016 Mikhail Baynov. All rights reserved.
//

#import "AppDelegate.h"

#include "wallet.h"

@interface AppDelegate ()

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {

   main_runloop();
   
   return YES;
}


@end
